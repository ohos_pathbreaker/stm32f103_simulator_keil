# 鸿蒙轻内核LiteOS-M移植支持STM32F103  Keil Simulator开发板

#### 介绍

该仓包含鸿蒙轻内核LiteOS-M移植支持`STM32F103RB`Keil MDK模拟器工程的代码。需要的Keil MDK版本为 V 5.28.0.0。

#### 一、Keil MDK编译

需要确保已经安装好`Keil MDK`。

1. 执行命令`git clone https://gitee.com/openharmony/kernel_liteos_m.git` 检出`kernel-liteos-m`代码。假设检出目录为`~/kernel-liteos-m`。

2. 在本地目录，执行 `git clone https://gitee.com/rtos_lover/stm32f103_simulator_keil.git` 检出代码，假设检出目录为`~/stm32f103_simulator_keil`。 执行如下命令把检出工程的`stm32f103_simulator_keil`文件夹复制到`kernel-liteos-m`工程的`targets`文件夹下。

```shell
cp -r ~/stm32f103_simulator_keil ~/kernel-liteos-m/targets/stm32f103_simulator_keil
```

3. 在本地目录，检出`bounds_checking_function`、`cmsis`等三方组件，假设检出目录`~/third_party`。

```shell
mkdir ~/third_party

git clone https://gitee.com/openharmony/third_party_bounds_checking_function.git  ~/third_party/bounds_checking_function

git clone https://gitee.com/openharmony/third_party_cmsis.git  ~/third_party/cmsis
```

4. 切换到目录`~/kernel-liteos-m/targets/stm32f103_simulator_keil/project`，双击文件`los_demo.uvprojx`即可启动`Keil MDK`软件，即可开始编译、烧录、调测运行。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

